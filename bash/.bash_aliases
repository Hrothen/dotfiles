# Shared aliases for all machines
# Machine specific aliases should go in .bash_aliases_local


# enable color support of ls
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls -la --color=auto'
else
    # We're in BSD land, set CLICOLOR instead
    export CLICOLOR=1
    alias ls='ls -la'
fi


# Grep
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'


# some more ls aliases
alias ll='ls -alF'
alias la='ls -A'
alias l='ls -CF'


# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'


# Git and Tig
alias gg='git grep'
alias ga='git add'
alias gl='git log'
alias tl='tig log'


# Github
alias github='hub'

# Stack
alias stack='stack --color=always'
