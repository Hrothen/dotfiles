# This is only read by login shells

# Source a local bash profile
[[ -r $HOME/.bash_profile_pre ]] && source $HOME/.bash_profile_pre

#just source .bashrc
[[ -r $HOME/.bashrc ]] && source $HOME/.bashrc

# Things that need to go into the profile after the bashrc
# not likely to be used by anything but OSX
[[ -r $HOME/.bash_profile_post ]] && source $HOME/.bash_profile_post

# Start in an xsession
if [ -z "$DISPLAY" ] && [ -n "$XDG_VTNR" ] && [ "$XDG_VTNR" -eq 1 ]; then
  exec startx
fi
if [ -e /home/leif/.nix-profile/etc/profile.d/nix.sh ]; then . /home/leif/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer
