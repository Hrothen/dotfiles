#!/bin/bash
# vi: ft=sh :

if tput setaf 1 &> /dev/null; then
  tput sgr0
  # if [[ $(tput colors) -ge 256 ]] 2>/dev/null; then
  #   RED=$(tput setaf 196)
  #   GREEN=$(tput setaf 190)
  #   ORANGE=$(tput setaf 214)
  #   PURPLE=$(tput setaf 141)
  #   CYAN=$(tput setaf 123)
  # else
    RED=$(tput setaf 1)
    GREEN=$(tput setaf 2)
    ORANGE=$(tput setaf 3)
    PURPLE=$(tput setaf 5)
    CYAN=$(tput setaf 6)
#  fi
  WHITE=$(tput setaf 7)
  BOLD=$(tput bold)
  RESET=$(tput sgr0)
  BLUE=$(tput setaf 4)
  YELLOW=$(tput setaf 11)
else
  RED=''
  GREEN=''
  ORANGE=''
  PURPLE=''
  CYAN=''
  WHITE=''
  BOLD=''
  RESET=''
  BLUE=''
  YELLOW=''
fi

function bash_prompt_nonzero_exitcode() {
  local RETVAL=$?
  [ $RETVAL -ne 0 ] && \
    echo " exit:${RETVAL}"
}

function bash_prompt_jobs_running() {
  local -a JOBS=($(jobs -p))
  local JOBCOUNT=${#JOBS[@]}
  [ $JOBCOUNT -ne 0 ] && \
    echo " jobs:${JOBCOUNT}"
}

# Enable git repository info
[[ ! $(type -t __git_ps1) ]] && source $HOME/.config/shell-scripts/git-prompt-info.sh

export GIT_PS1_SHOWDIRTYSTATE=1
export GIT_PS1_SHOWCOLORHINTS=1

# See bash(1) for description. Part of bash v4.
export PROMPT_DIRTRIM=3

if locale | grep -q UTF-8; then
  ps_leading="\342\227\200\357\270\216"
  ps_trailing="\342\226\266\357\270\216"
else
  ps_leading="<"
  ps_trailing=">"
fi

PS1=''
#PS1+="\[${BOLD}${PURPLE}\]${ps_leading}\[${RESET}\]"
if [ $UID -eq 0 ]; then
  PS1+="\[${ORANGE}\]\u"
else
  PS1+="\[${CYAN}\]\u"
fi
PS1+="\[${RESET}\]@"
PS1+="\[${GREEN}\]\h:"
#PS1+=" "
#PS1+="\[\033[33:1m\]\w"
PS1+="\[${YELLOW}\]\w"
PS1+="\[${RED}\]\$(bash_prompt_nonzero_exitcode)"
PS1+="\[${BLUE}\]\$(bash_prompt_jobs_running)"
PS1+="\[${RESET}\]\$(__git_ps1 \" (%s)\" )"
PS1+="\[${RESET}\] $"
PS1+=" "
