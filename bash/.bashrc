# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000000
HISTFILESIZE=2000000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"


# Common alias definitions.
[[ -f $HOME/.bash_aliases ]] && source $HOME/.bash_aliases
# Local alias definitions
[[ -f $HOME/.bash_aliases_local ]] && source $HOME/.bash_aliases_local


# enable programmable completion features
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  elif [[ $(type -t brew) ]] && [[ -f $(brew --prefix)/etc/bash_completion ]]; then
    source $(brew --prefix)/etc/bash_completion
  fi
fi


# Color the prompt
[[ -r $HOME/.config/shell-scripts/bash-prompt.sh ]] && source $HOME/.config/shell-scripts/bash-prompt.sh


PATH=$PATH:$HOME/.local/bin
PATH=$PATH:$HOME/.cargo/bin
# go binary locations
PATH=$PATH:/usr/local/opt/go/libexec/bin
GOPATH=$HOME/go
PATH=$PATH:$GOPATH/bin

if [[ $TERM == xterm-termite ]]; then
  . /etc/profile.d/vte.sh
  __vte_prompt_command
fi
