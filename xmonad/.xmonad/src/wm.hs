module Main where

import System.Exit

import XMonad
import XMonad.Config.Desktop
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Layout.NoBorders
import XMonad.Prompt
import XMonad.Prompt.ConfirmPrompt
import XMonad.Prompt.Shell
import XMonad.Util.EZConfig

myConfig = desktopConfig
  { terminal   = "termite"
  , modMask    = mod4Mask
  , layoutHook = smartBorders $ layoutHook desktopConfig
  , logHook    = dynamicLogString def >>= xmonadPropLog
  }
  `additionalKeysP`
    [ ("M-S-q", confirmPrompt myXPConfig "exit" (io exitSuccess))
    , ("M-p",   shellPrompt myXPConfig)
    , ("M-S-f", spawn "apulse firefox")
    ]

myXPConfig :: XPConfig
myXPConfig = def
  { position          = Top
  , alwaysHighlight   = True
  , promptBorderWidth = 0
  , font              = "Source Code Pro Light:monospace:size = 9"
  }

main :: IO ()
main = do
  spawn "vmware-user"
  xmonad =<< statusBar myBar myPP toggleStrutsKey (ewmh myConfig)

myBar = "xmobar"

myPP = xmobarPP
  { ppCurrent = xmobarColor "#bf616a" ""
  , ppHidden = xmobarColor "#c0c5ce" ""
  , ppHiddenNoWindows = xmobarColor "#4f5b66" ""
  , ppUrgent = xmobarColor "#a3be8c" ""
  , ppLayout = xmobarColor "#4f5b66" ""
  , ppTitle =  xmobarColor "#c0c5ce" "" . shorten 80
  , ppSep = xmobarColor "#4f5b66" "" "  "
  }

toggleStrutsKey XConfig {XMonad.modMask = modmask} = (modmask, xK_b)
