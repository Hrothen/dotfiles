syntax on
filetype plugin indent on

" Use ',' as the leader
let mapleader = ','
" But still be able to get ',' by pressing it twice quickly
noremap ,, ,
" Use a 2 second timeout
set tm=2000


set nocompatible
set ruler
set number
set nowrap
set showmode
set tw=0
set smartcase
set smarttab
set smartindent
set autoindent
set softtabstop=2
set shiftwidth=2
set expandtab
set incsearch
set hlsearch
set mouse=a
set clipboard=unnamedplus,autoselect
set hidden
set autoread

set diffopt+=vertical

set completeopt=menuone,menu,longest

set wildignore+=*\\tmp\\*,*.swp,*.swo,*.zip,.git,.cabal-sandbox
set wildmode=list:longest,full
set wildmenu
set completeopt+=longest

set laststatus=2
set cmdheight=1

set nomodeline
set backspace=indent,eol,start
set whichwrap+=<,>,h,l,[,]

" Show trailing whitespace
set list
if &listchars ==# 'eol:$'
  set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
endif

" Don't redraw while executing macros
set lazyredraw

" Disable the visual and audio bells
set noerrorbells
set vb t_bv=

" Plugins
call plug#begin('~/.vim/bundle')
Plug 'eagletmt/ghcmod-vim', { 'for': 'haskell' }
Plug 'eagletmt/neco-ghc', { 'for': 'haskell' }
Plug 'ctrlpvim/ctrlp.vim'
Plug 'neovimhaskell/haskell-vim'

"syntastic doesn't like ghc-mod, so use neomake for haskell
Plug 'scrooloose/syntastic'
Plug 'benekastah/neomake', { 'for': 'haskell' }

Plug 'nanotech/jellybeans.vim'

" Airline
if v:version >= 702
  Plug 'vim-airline/vim-airline'
  Plug 'vim-airline/vim-airline-themes'
  let g:airline_powerline_fonts = 1
  let g:airline_theme='jellybeans'
  let g:airline#extensions#tabline#enabled = 1
  set noshowmode
endif


" Snippets
if has('python') || has('python3')
  Plug 'SirVer/ultisnips'
  let g:UltiSnipsEnableSnipmate=1
else
  Plug 'tomtom/tlib_vim'
  Plug 'MarcWeber/vim-addon-mw-utils'
  Plug 'garbas/vim-snipmate'
endif
Plug 'honza/vim-snippets'

Plug 'tpope/vim-commentary'
Plug 'godlygeek/tabular'
Plug 'ervandew/supertab'
" formatting
Plug 'tpope/vim-surround'
Plug 'nathanaelkane/vim-indent-guides'

Plug 'tpope/vim-fugitive'

Plug 'Shougo/vimproc.vim', {'do': 'make' }

" Rust
Plug 'rust-lang/rust.vim'
Plug 'racer-rust/vim-racer'

" Go
Plug 'fatih/vim-go', { 'for': 'go' }

" colorschemes
Plug 'NLKNguyen/papercolor-theme'
Plug 'gilgigilgil/anderson.vim'
Plug 'mkarmona/colorsbox'
call plug#end()


" Enable truecolor if we have it, otherwise use 256 colors
if has('termguicolors')
  " The "^[" is a single character. You enter it by pressing Ctrl+v and then ESC.
  set t_8f=[38;2;%lu;%lu;%lum
  set t_8b=[48;2;%lu;%lu;%lum
  set termguicolors
else
  set t_Co=256
endif
set background=dark
colorscheme jellybeans

if exists('*getmatches')
  let g:syntastic_error_symbol              = '✗✗'
  let g:syntastic_warning_symbol            = '⚠⚠'
  let g:syntastic_style_error_symbol        = '✗'
  let g:syntastic_style_warning_symbol      = '⚠'
  let g:syntastic_auto_loc_list             = 1
  let g:syntastic_loc_list_height           = 5
  let g:syntastic_sh_checkers               = ['shellcheck', 'checkbashisms', 'sh']
  let g:syntastic_sh_checkbashisms_args     = '-x'
  let g:syntastic_xml_checkers              = ['xmllint']
  let g:syntastic_xslt_checkers             = ['xmllint']
  let g:syntastic_go_checkers               = ['golint', 'govet', 'errcheck']
  let g:syntastic_mode_map = { 'mode': 'active', 'passive_filetypes': ['go'] }
endif

let g:syntastic_stl_format = '[%E{Err: %fe #%e}%B{, }%W{Warn: %fw #%w}]'

" autocmd FileType haskell map <silent> <leader><cr> :noh<cr>:GhcModTypeClear<cr>
nmap <silent> <leader>hc :Neomake ghcmod<CR>
nmap <silent> <leader>hl :Neomake hlint<CR>
let g:neomake_haskell_ghc_mod_args = ['-g', '-Wall']
"let g:neomake_haskell_hlint_args = ["--ignore='Use if'"]
let g:neomake_open_list = 2

" ----------------------------------------------------------------------------
"  Backups, Swap, and Undo
" ----------------------------------------------------------------------------

" If .vim-backup exists in the current directory, backup there,
" otherwise backup in ~/.vim/backup, or . if that also doesn't exist.
if isdirectory('~/.vim/backup') == 0
  execute 'silent !mkdir -p ~/.vim/backup >/dev/null 2>&1'
endif
set backupdir=.vim-backup/,~/.vim/backup/,.
set backup

" Prevent backups from overwriting each other
if has('autocmd')
  augroup Backups
    autocmd!
    autocmd BufWritePre * nested let &backupext = substitute(expand('%:p:h'), '/', '%', 'g') . '~'
  augroup END
endif

if has('macunix')
  set backupskip+=/private/tmp/*
endif

" Store swapfiles in the first of .vim-swap, ~/.vim/swap, ~/tmp, or .
if isdirectory('~/.vim/swap') == 0
  execute 'silent !mkdir -p ~/.vim/swap >/dev/null 2>&1'
endif

set directory=./.vim-swap//,~/.vim/swap//,~/tmp//,.

" Store the state of the previous editing session in ~/.vim/viminfo
set viminfo+=n~/.vim/viminfo
set viminfo^=!,h,f0,:100,/100,@100

" Create an undofile in .vim-undo or ~/.vim/undo
if exists('+undofile')
  if isdirectory('~/.vim/undo') == 0
    execute 'silent !mkdir -p ~/.vim/undo >/dev/null 2>&1'
  endif
  set undodir=./vim-undo/,~/.vim/undo/
  set undofile
  set undolevels=1000 " Max number of changes to undo
  set undoreload=10000 " Max number of lines to save for undo on a reload
endif

" If possible, jump to the last known cursor position when editing a file
if has('autocmd')
  augroup OnLoad
    autocmd!
    autocmd BufReadPost * nested
          \ if line("'\"") > 1 && line("'\"") <= line("$") |
          \   exe "normal! g`\"" |
          \ endif
  augroup END
endif

" ----------------------------------------------------------------------------
"  Navigation
" ----------------------------------------------------------------------------

" make moving around splts a bit quicker
noremap <c-h> <c-w>h
noremap <c-k> <c-w>k
noremap <c-j> <c-w>j
noremap <c-l> <c-w>l

" ----------------------------------------------------------------------------
"  highlighting
" ----------------------------------------------------------------------------

" let g:hs_highlight_types = 1
" let g:hs_highlight_boolean = 1
" let g:hs_highlihgt_debug = 1
" let g:hs_highlight_delimiters = 1
" let g:hs_allow_hash_operator = 1

" ----------------------------------------------------------------------------
"  ghc-mod
" ----------------------------------------------------------------------------

map <silent> <leader> hT :GhcModTypeInsert<CR>
map <silent> <leader> ts :GhcModSplitFunCase<CR>
map <silent> <leader> ht :GhcModType<CR>
map <silent> <leader> te :GhcModTypeClear<CR>

" ----------------------------------------------------------------------------
"  Tab Completion
" ----------------------------------------------------------------------------

" Supertab
let g:SuperTabDefaultCompletionType = "context"

if has("gui_running")
  imap <c-space> <c-r>=SuperTabAlternateCompletion("\<lt>c-x>\<lt>c-o>")<cr>
else "no gui
  if has("unix")
    inoremap <Nul> <c-r>=SuperTabAlternateCompletion("\<lt>c-x>\<lt>c-o>")<cr>
  endif
endif

" necoghc
let g:haskellmode_completion_ghc = 0
augroup Haskell
  autocmd!
  autocmd FileType haskell setlocal omnifunc=necoghc#omnifunc
  autocmd FileType haskell setlocal formatprg=stack\ exec\ brittany
augroup END

" ----------------------------------------------------------------------------
"  Completion tools
" ----------------------------------------------------------------------------

" ----------------------------------------------------------------------------
"  tabularize
" ----------------------------------------------------------------------------

let g:haskell_tabular = 1

vmap a= :Tabularize /=<CR>
vmap a; :Tabularize /::<CR>
vmap a- :Tabularize /-><CR>
vmap a\| :Tabularize /\|<CR>

" ----------------------------------------------------------------------------
"  Indenting
" ----------------------------------------------------------------------------

"disable haskell-vim indenting
let g:haskell_indent_disable = 1
" let g:indent_guides_auto_colors = 1
" let g:haskell_indent_if = 0
" let g:haskell_indent_case = 2
" let g:haskell_indent_let = 2
" let g:haskell_indent_where = 2
" let g:haskell_indent_do = 2
" let g:haskell_indent_in = 0
" let g:haskell_indent_guard = 0

" ----------------------------------------------------------------------------
"  Ctrl-p
" ----------------------------------------------------------------------------

map <silent> <leader><space> :CtrlP()<CR>
noremap <leader>b<space> :CtrlPBuffer<cr>
let g:ctrlp_custom_ignore = '\v[\/]dist$'

" ----------------------------------------------------------------------------
"  haskell-vim
" ----------------------------------------------------------------------------

let g:haskell_enable_quantification = 1
let g:haskell_enable_recursivedo = 1
let g:haskell_enable_arrowsyntax = 1
let g:haskell_enable_pattern_synonyms = 1
let g:haskell_enable_typeroles = 1
let g:haskell_enable_static_pointers = 1

" ----------------------------------------------------------------------------
"  rust
" ----------------------------------------------------------------------------

let g:rustfmt_autosave = 1
let $RUST_SRC_PATH="$HOME/rust/rust-lang/src"

" ----------------------------------------------------------------------------
"  go
" ----------------------------------------------------------------------------

" format go files a little differently
if has('autocmd')
  augroup GoLang
    autocmd!
    autocmd FileType go nested setlocal wrap noexpandtab listchars+=tab:\ \  nolist
  augroup END
endif

" vim-go settings
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_structs = 1
let g:go_highlight_interfaces = 1
let g:go_highlight_operators = 1
let g:go_highlight_build_constraints = 1
let g:go_fmt_command = "goimports" " goimports does what gofmt does, but also fixes imports
let g:go_fmt_autosave = 1

